# Kubernetes

Neste repositório encontra-se os arquivos de configuração para fazer o deploy da aplicação no Kubernetes.

A estratégia de deploy do tipo *RollingUpdate* foi adotado para ambas as aplicações, back-end e front-end.

## Pré-requisitos

- Minikube ou qualquer provedor de nuvem pública com Kubernetes
- Kubectl

## Executando o Back-end

Crie os container no Kubernetes através do arquivo `backend-deployment.yml` com o comando abaixo:

```bash
kubectl create -f backend/backend-deployment.yml
```

Verifique se os Pods foram criados

```bash
kubectl get pods
```

Crie o *LoadBalancer*.

```bash
kubectl create -f backend/backend-service.yml
```

## Executando o banco de dados

Crie o *Pod* e o *LoadBalancer* do banco de dados com os comandos abaixo:

```bash
kubectl create -f database/couchdb-pod.yml

kubectl create -f database/couchdb-service.yml
```

## Executando o front-end

Antes da criação dos serviços no Kubernetes para a aplicação do front-end é necessário criar novamente a aplicação com o endereço IP do LoadBalancer da aplicação do back-end. Para isso execute o comando abaixo e recupere o endereço IP do serviço `backend-service`.

```bash
kubectl get services
```

Agora altere a variável de ambiente dentro do arquivo `Dockerfile` da aplicação do front-end.

```bash
FROM node:lts-alpine

RUN npm install -g http-server

ENV URL_API <IP-SERVICE-BACKEND>
```

Agora faça o commit da alteração no arquivo no repositório do GitLab. Uma nova imagem será gerado com o novo endereço.

Após a geração da imagem, podemos criar os container no Kubernetes através do arquivo `frontend-deployment.yml` com o comando abaixo:

```bash
kubectl create -f frontend/frontend-deployment.yml
```

Verifique se os Pods foram criados

```bash
kubectl get pods
```

Crie o *LoadBalancer*.

```bash
kubectl create -f backend/backend-service.yml
```

Antes de acessar a aplicação devemos descubrir o IP da aplicação do front-end. Para isso, execute o comando abaixo e procure pelo IP do serviço `frontend-service`.

```bash
kubectl get services
```

Por fim, abra um navegador de preferência e digite o IP recuperado anteriormente.